@login
Feature: Login to application

Scenario:
Verify the homepage of the application after logging into it.

Given User is on application login page.
When User enters the valid login credentials and login into the app.
Then He should be navigated to home page of the application. 