package Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class loginPage {

	@FindBy(id = "username")
	public WebElement userName;

	@FindBy(id = "password")
	public WebElement password;

	@FindBy(id = "Login")
	public WebElement loginbtn;

	@FindBy(xpath = "//span[@title='Service Console']")
	private WebElement header;

	protected static WebDriver driver;
	public loginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
	}

	public void getUrl() {
		driver.get("https://balsambrands--automation.lightning.force.com/");
	}

	public void appLogin() {

		userName.sendKeys("rinky.chhatwani@intimetec.com.automation");
		password.sendKeys("rinky@123");
		loginbtn.click();
	}

	public boolean hompageTitle() {
		
		boolean flag=false;
		if(header.isDisplayed())
		{
			flag=true;
			System.out.println("User landed on home page of the application");
		}
		return flag;
		
	}
}
