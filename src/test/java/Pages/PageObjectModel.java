package Pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PageObjectModel {
	
	protected loginPage objLoginPage;
	WebDriver driver;
	public PageObjectModel() throws IOException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+
                "\\src\\main\\webdriver\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		objLoginPage = new loginPage(driver);
		 
	}
}
