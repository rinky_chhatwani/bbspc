package StepDefinition;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(monochrome = true, features = { "src/test/java/feature" }, glue = {
		"StepDefinition", "TestRunner"}, tags = {
				"@login" }, plugin = { "pretty", "html:target/cucumber-reports/cucumber-pretty",
						"json:target/cucumber-reports/CucumberTestReport.json" })

public class TestRunner extends AbstractTestNGCucumberTests {

}
