package StepDefinition;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.PageObjectModel;
import Pages.loginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class LoginSteps extends PageObjectModel {
	
	public LoginSteps() throws IOException {
		//super();
		// TODO Auto-generated constructor stub
	}

	
	

	@Given("User is on application login page.")
	public void user_is_on_application_login_page() {
	   
		/*System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+
                "\\src\\main\\webdriver\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();*/
		objLoginPage.getUrl();
        System.out.println("This Step open chrome and launch the application.");
	}

	@When("User enters the valid login credentials and login into the app.")
	public void user_enters_the_valid_login_credentials_and_login_into_the_app() {
		objLoginPage.appLogin();
	
	    
	}

	@Then("He should be navigated to home page of the application.")
	public void he_should_be_navigated_to_home_page_of_the_application() {
		objLoginPage.hompageTitle();
		
		//referencePage.login().hompageTitle();
	   
	}
}
